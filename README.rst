=====================================
My personal configuration of Emacs 23
=====================================

Install::

  1- download all files 
  2- install the packages and run the command from install.txt
  3- put the files on your home directory (or run install.sh) 
  4- run emacs

It has::

  Rsense (Rsense)
  rinari (ruby on rails)
  Yasnippet
  Autocomplete
  Flymake (ruby, html, latex)
  MuMaMo
  nXhtml
  Scss


In .emacs file, you will find more information about packages needed install.

`David <https://bitbucket.org/dpzaba>`_
======================================================================================================



